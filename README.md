# Dépôt officiel de la licence OQP

## Description

Ce dépôt contient le site principal de la licence OQP et une copie du texte de la licence.

## Construction du site

### Dépendances pour construire le site

 * Ruby avec support des *gems*
 * Nanoc + ADSF (pour le rendu)
 * Kramdown (support du Markdown)
 * Webrick (serveur Web de test)

### Commandes à lancer

 * Fedora GNU/Linux :
```sh
$ sudo dnf install ruby ruby-devel
$ sudo gem update
$ sudo gem install nanoc adsf kramdown webrick
```

## Licence du projet

Licence OQP
