---
title: Accueil
lang: fr
---
## Présentation
OQP ( \o.ky.pe\ ) est une nouvelle licence libre et open-source. Elle est compatible avec les autres licences libres et elle fonctionne sur le même principe que ces dernières.  
Bien que non reconnue par la FSF pour le moment, elle est conçue pour être compatible avec la démarche de cette organisation.

## FAQ

### Est-ce une vraie licence ?

Oui, la licence OQP est une vraie licence. Elle fonctionne selon le même principe qu'une licence de type BSD-Clause 2.

### Puis-je utiliser cette licence pour mon projet ?

Oui !

### Est-elle reconnue légalement ?

Pour le moment, cette licence n'est pas reconnue officiellement, mais cela n'empêche pas de l'utiliser pour des travaux sérieux et légitimes. Ce site l'utilise lui-même.

### D'où vient ce nom ?

OQP est né d'une blague à la base : ma petite copine voulait essayer de faire un jeu de mots avec le mot libre que l'on trouve dans Logiciel Libre par exemple. Elle a eu alors l'idée de faire un truc avec le mot occupé et j'ai alors suggéré qu'on peut faire facilement une licence avec un acronyme se prononcant comme "occupé".
