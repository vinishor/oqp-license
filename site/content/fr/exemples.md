---
title: Exemples
lang: fr
---
## Exemples d'utilisation

Voici une liste complète des projets utilisant la licence OQP :

 * [Le site de la licence OQP](https://oqp.ovh)

Pour contribuer à cette liste, vous pouvez nous envoyer un [e-mail](mailto:vincent.finance@linuxmario.net) ou bien une [Merge Request](https://framagit.org/vinishor/oqp-license/-/merge_requests/new) sur notre dépôt Git.
