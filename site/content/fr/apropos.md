---
title: À propos de la license OQP
lang: fr
---
## Texte de licence complet

~~~
LICENCE PUBLIQUE OQP
Version 1, Janvier 2022

Copyright (C) 2022 <AUTEUR> <ADRESSE MAIL>

LICENCE PUBLIQUE OQP
CONDITIONS GÉNÉRALES DE COPIE, DISTRIBUTION ET DE MODIFICATION

  1. Les redistribution de toute nature doivent contenir la mention de droit de 
  copie ci-dessus et cette liste de conditions.
  2. La redistribution et l'usage sous tout forme, sans modifications, est 
  permise.
  3. La redistribution et l'usage sous tout forme, avec modifications, est 
  permise tant que l'auteur original est mentionné avec une phrase spéciale :
  CE TRAVAIL EST BASÉ SUR <nom du projet> DE <nom de l'auteur>, PUBLIÉ SOUS 
  LICENCE OQP.
  4. Ni l'auteur original et/ou ses contributeurs peuvent être utilisés pour 
  promouvoir ou valider les produits et les travaux dérivés de ce projet, sans 
  autorisation écrite spécfique. Les gens sont assez occupés comme ça.
  5. Le projet et ses travaux sont fournis "TELS QUELS" et aucune garantie 
  quelconque, explicite ou implicite, n'est donnée. Les gens sont assez occupés
  comme ça.
  6. Les gens sont assez occupés pour vous faire confiance sur le bon respect de
  ces conditions.
~~~

Vous pouvez télécharger une [version texte](/txt/COPYING.FR) pour l'inclure dans vos projets.
