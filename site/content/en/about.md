---
title: About the OQP license
lang: en
---
## Full license text

~~~
OQP PUBLIC LICENSE
Version 1, January 2022

Copyright (C) 2022 <AUTHOR'S NAME> <MAIL ADDRESS>

OQP PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  1. Redistributions in any form must retain the above copyright notice and this
 list of conditions and terms.
  2. Redistribution and use in any form, without modification, is permitted.
  3. Redistribution and use in any form, with modification, is permitted as long
 as the original author is mentionned with a special sentence : THIS WORK IS 
 BASED ON <project's name> BY <author's name>, PUBLISHED UNDER OQP LICENSE.
  4. Neither the original author and/or its contributors may be used to endorse 
 or promote products or works derived from this project without specific prior 
 written permission. Everyone is busy enough.
  5. This project and its works are provided 'AS IS' and no express or implied 
 warranties of any kind are given. Everyone is busy enough.
  6. People are busy enough to trust you in order to respect these terms and 
 conditions.
~~~

You can download a [plain text version](/txt/COPYING.EN) for inclusion in your projects.
