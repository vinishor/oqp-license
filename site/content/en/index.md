---
title: Home
lang: en
---
## Presentation

OQP ( \ˈɑk.jə.ˌpaɪ\ ) is a new open-source and free (as in free software) license. It is compatible with the others types of free licenses, and it uses the same principles.  
Even if the FSF doesn't approve it for now, this license has been made to be compatible with their licenses.

## FAQ

### Is it a real license?

Yes, the OQP license is a real license. It is based on the BSD-Clause 2 license.

### Can I use this license for my project?

Yes!

### Is it legally supported?

For now, this license is not officially recognized, however, you can use it for serious works and projects. This very website uses it.

### Where does the name come from?

OQP has been born from a joke : it is a play on words made by my girlfriend, since I talk about free software time to time, and she joked about the fact that there is no "busy license". So, I decided to create a free (as in free software) license for busy people. Interestingly, "occupy" and "occuper" share a similar meaning in French and in English.
